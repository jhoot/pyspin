import nltk
from nltk.corpus import wordnet
from nltk.tokenize import word_tokenize

f = open("wordlist.txt", "r")

wordlist = f.read()

words = word_tokenize(wordlist)

# print(words)

def createArray(w):

	syn = wordnet.synsets(w)

	swaps = []
	fswaps = []

	# grab synonyms
	for s in syn:
		for l in s.lemmas():
			swaps.append(l.name())

	# remove duplicates
	fswaps = set(swaps)

	final = ""

	# check for first element
	first = True

	for each in fswaps:
		if first:
			final = final+"'"+each+"'"
			first = False
		else:
			final = final+", '"+each+"'"

	array = ("$"+w+" = new array("+final+");")

	return array

file = open("arrays.txt", "w+")

for item in words:
	file.write(createArray(item)+"\n")

file.close()
