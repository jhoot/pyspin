words = []

new_word = ""

while new_word != 'quit':
	new_word = input('Need a synonym array? (enter "quit" to exit)')
	if (new_word != 'quit'):
		words.append(new_word)
	
strwords = ' '.join(words)

f = open("wordlist.txt", "w+")

f.write(strwords)

f.close()
